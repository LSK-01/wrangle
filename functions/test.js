//testing wrapper
exports.testingFunctionGiveWrangles = functions.https.onRequest((req, res) => {

   
    let promise = firestore.collection('arguments').where('endingAt', '==', 0).get()
    .then(snapshot => {

        if (!snapshot.empty){
            const promises = []

            snapshot.forEach(arg => {
                let document = arg.data()

                //deincrement usersFor and against on topic
                var topicTitle = document.topicTitle
                var argumentID = arg.id

                if (document.upvotesAgainst < document.upvotesFor){
                    var winningUser = document.uids.for
                    var wranglesToReward = document.upvotesFor - document.upvotesAgainst

                }
                else{
                    var winningUser = document.uids.against
                    var wranglesToReward = document.upvotesAgainst - document.upvotesFor
                }

                    console.log("rewarding " + wranglesToReward + " wrangles to " + winningUser)

                        let userDocRef = admin.firestore().doc('users/' + winningUser)
                        let updateUser = userDocRef.update({
                            "wrangles": FieldValue.increment(wranglesToReward),
                            "wins": FieldValue.increment(1)
                        })

                        let fieldNameUsersFor = "usersFor." + document.uids.for
                    let fieldNameUsersAgainst = "usersAgainst." + document.uids.against
                        var data = {}
                        data[fieldNameUsersFor] = FieldValue.delete()
                        data[fieldNameUsersAgainst] = FieldValue.delete()
                        data["numUsersForTotal"] = FieldValue.increment(-1)
                        data["numUsersAgainstTotal"] = FieldValue.increment(-1)
                        let updateTopic = admin.firestore().doc('topics/' + topicTitle).update(data)

                        data = {}
                        console.log("penis!!")
                        console.log(winningUser)
                        data["winner"] = document[winningUser]["username"]
                        data["uidsForQuerying"] = {
                            
                            [document.uids.for] : true, 
                            [document.uids.against] : true
                        }
                        
                        let archiveArg = admin.firestore().doc('arguments/' + argumentID).update(data)

                    promises.push(updateUser)
                    promises.push(updateTopic)
                    promises.push(archiveArg)

            })
            res.send(Promise.all(promises))
        }
        else{
            res.send(promise);
        }

    }).catch(err => {
        console.log(err)
        res.send(promise);
    })
    })

    exports.testingFunction2 = functions.https.onRequest((req,res) => {

        let query = firestore.collection('arguments').where('goingPublicAt', '==', 1628038800).get()

        let promise = query.then(snapshot => {
             if (!snapshot.empty){
            snapshot.forEach(arg => {

                let argInfo = arg.data()

                if (argInfo.latestMessage == null){
                    let uidFor = argInfo.uids.for
                    let uidAgainst = argInfo.uids.against
                    
                    var uidsForQuerying = {
                        [uidFor]: true,
                        [uidAgainst]: true
                    }

                    firestore.doc('arguments/' + arg.id).update({uidsForQuerying})
                    res.send(promise)
                }
                else{
                    firestore.doc('arguments/' + arg.id).update({
                        "isPublic": true,
                        "upvotesFor": 0,
                        "upvotesAgainst": 0,
                        "for": [],
                        "against":[],
                        "totalUpvotes": 0,
                        "endingAt": startOfLastHourInSecs + (3 * 3600)}
                      )
                      res.send(promise)
                }
                
            })
            //remove user from topic array here

        }
            res.send(promise)
    
        }).catch(err => {
            console.log(err)
            res.status(500).end()
        })
    }
    )